# Java Coding Challenges

Below are a series of tiny coding challenges between two friends for the purposes of reaquainting ourselves with the Java Language.

## Rules

1. You can use any Java Environment or IDE
2. You can use online resources but should not copy and paste code
3. There are no winners or loosers, this is purely for the excercise of coding
4. No rules are strictly enforced, we use the honor system

## Challenges

### #1 Hello, World!

1. Setup your development environment (any environment is fine NetBeans, Visual Studio, command line, etc.)                        
2. Write a valid program that prints "Hello, World!" on the command line                        
3. Successfully compile and run your hello world program

### #2 Hello, You!

1. Prompt the user to enter their name                        
2. Accept the user's input and store it in a variable                        
3. Finally, print "Hello {user's input}" on the command line

### #3 Multiples of 3 and 5

*Source: Project Euler - Problem 1*

If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
