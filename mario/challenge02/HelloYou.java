import java.io.*;
import java.util.Scanner;

public class HelloYou {

  public static void main(String[] args) {
    System.out.print("Please enter your name: ");
    Scanner scanner = new Scanner(System.in);
    String username = scanner.nextLine();
    System.out.println(String.format("Hello, %s!", username));
  }

}
