import java.io.*;

public class MultiplesOfThreeAndFive {

  public static final int MAX = 1000;

  public static void main(String[] args) {

    int sum = 0;

    for (int i=1; i<MAX; i++) {
      if (i % 3 == 0 || i % 5 == 0) {
        sum += i;
      }
    }

    System.out.println(String.format("Answer: %d", sum));
  }

}
